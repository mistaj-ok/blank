//size coords 
const NUM_PRODUCTS = 44;
var lower = 0;
var upper = 0;
// parent node to append to
var content = document.querySelector('.scroller');
var grid = document.createElement('div');
grid.classList.add('grid');

/**
 * generate 2 operands to use for the multiplicaiton data
 * @param {the upper limit for operands} upper 
 */
function makeProd(low,up) {
    console.log("low: " + low + " up: " + up);
    this.lower = parseInt(low);
    this.upper = parseInt(up);
    console.log("lower: " + lower +  " is instance of: " + typeof lower);
    if (Number.isInteger(Number(lower)) && Number.isInteger(upper)) {        
        console.log("range: " + lower + " -> " + upper);
        const numA = Math.floor(Math.random() * (upper - lower) + lower);
        const numB = Math.floor(Math.random() * (upper - lower) + lower );
        console.log("numA: " + numA + "\nnumB: " + numB);

        // new product 'box' generated 
        div = document.createElement('div');
        div.classList.add("box");
        operandA = document.createElement('div');
        operandB = document.createElement('div');
        ansSpace = document.createElement('div');

        operandA.classList.add("operand");
        operandB.classList.add("operand");
        operandB.classList.add("operand-line")
        ansSpace.classList.add("answer-space")

        operandA.textContent= numA;
        operandB.textContent = "x________"  +  numB ;



        div.append(operandA);
        div.append(operandB);
        div.append(ansSpace);

        // set content

        grid.appendChild(div);
    }
    else {
        // alert("NaN");
        console.log("... not a number.");
    }
}

/**
 * populateProblems
 * calls the productGen function and inserts the
 * sub-module into the grid (container)
 */
function populateProducts() {
  
    for (let i=0; i<NUM_PRODUCTS; i++) { 
        console.log("low: " + lower + "\nupper:" + upper);
        makeProd(lower, upper);
    }
    content.append(grid)
}

function populateSquares(root) {
    for (let i=0; i<NUM_PRODUCTS; i++) { 
        var root = Math.floor(Math.random() * (upper - lower) + lower); 
        makeProd(root, root);
    }
    content.append(grid)
}

/**
 * pulls operand data from html input elements\
 * called with 'go' button
 */
function getInputValue () {
    console.log("getInputValue()");
    lower = document.getElementById("lowOpInput").value;
    console.log("l: " + lower);
    upper = document.getElementById("upOpInput").value;
        console.log("u: " + upper);

    populateProducts();
}




// make & add boxes to container
    // var test = document.createElement('div');
    // console.log(test);
    // apply the multiplication box class
    // test.classList.add("box");
    // grid.append(test);




 // draw the boxes
// populateProducts();
// populateSquares();
 



 